﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab0.Main
{
    class Program
    {
        static void Main(string[] args)
        {

            List<Programowanie> ListaProgramow = new List<Programowanie> { };
            Programowanie p = new Programowanie();
            Strukturalne s = new Strukturalne();
            Obiektowe o = new Obiektowe();

            ListaProgramow.Add(p);
            ListaProgramow.Add(s);
            ListaProgramow.Add(o);

            foreach(var i in ListaProgramow)
            {
                Console.WriteLine("Typ " + i.GetType() + " matoda " + i.Napisz());
            }

            Console.WriteLine();
            Console.WriteLine("Naciśnij dowolny klawisz aby zamknac");

            Console.ReadKey();
            
        }
    }
}
