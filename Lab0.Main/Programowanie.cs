﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab0.Main
{
    class Programowanie
    {
        protected int iloscLinii;

        public Programowanie()
        {
        }

        public virtual int Napisz()
        {
            return 2;
        }
    }

    class Strukturalne : Programowanie
    {
        protected int iloscMetod;

        public override int Napisz()
        {
            return 1;
        }

    }

    class Obiektowe : Programowanie
    {
        protected int iloscKlas;

        public override int Napisz()
        {
            return 0;
        }
    }
}
